<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPricing extends Model
{
    protected $fillable = [
        'base_price',
        'date_created',
        'date_expiry',
        'is_active',
    ];
}
