<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
      protected $fillable = [
          'category_name',
          'max_reward_points_encash',
      ];

      public function products()
      {
          return $this->hasMany(Product::class);
      }

      public function discounts()
      {
          return $this->hasMany(ProductCategoryDiscount::class);
      }

      public function hasDiscounts()
      {
          return $this->discounts->isNotEmpty();
      }
}
