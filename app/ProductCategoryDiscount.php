<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryDiscount extends Model
{
    protected $fillable = [
        'discount_value',
        'discount_unit',
        'date_created',
        'valid_until',
        'coupon_code',
        'minimum_order_value',
        'maximum_discount_amount',
        'is_redeem_allowed',
    ];
}
