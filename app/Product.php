<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
      protected $fillable = [
          'product_name',
          'product_description',
          'units_in_stock',
          'reward_points_credit',
      ];

      public function category()
      {
          return $this->belongsTo(ProductCategory::class);
      }

      public function prices()
      {
          return $this->hasMany(ProductPricing::class);
      }

      public function discounts()
      {
          return $this->hasMany(ProductDiscount::class);
      }

      public function hasDiscounts()
      {
          return $this->discounts->isNotEmpty();
      }
}
