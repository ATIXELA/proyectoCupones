<?php

namespace App\Http\Controllers;

use App\ProductCategoryDiscount;
use Illuminate\Http\Request;

class ProductCategoryDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategoryDiscount  $productcategoryDiscount
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategoryDiscount $productCategoryDiscount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategoryDiscount  $productcategoryDiscount
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategoryDiscount $productCategoryDiscount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategoryDiscount  $productcategoryDiscount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategoryDiscount $productCategoryDiscount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategoryDiscount  $productcategoryDiscount
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategoryDiscount $productCategoryDiscount)
    {
        //
    }
}
