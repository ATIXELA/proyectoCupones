<?php

namespace App\Services\Template;

class DiscountDate extends TemplateAbstract
{

    protected function typeDiscount($couponcode): string
    {
        $typeDiscount = "";

        //consultar si el cupon tiene una cantidad maxima para aplicar el descuento
        $model=ProductCategoryDiscount::where('coupon_code',$couponcode)->first();
        if ($model->maximum_discount_amount!="")
        {
            $typeDiscount = "DiscountQuantity";
        }
         return $typeDiscount;
    }

}
