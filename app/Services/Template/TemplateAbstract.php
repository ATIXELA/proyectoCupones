<?php

namespace App\Services\Template;

use App\Product;
use Illuminate\Http\Request;

abstract class TemplateAbstract
{
    public function __construct(Product $product, Request $request)
    {
        $this->product = $product;
        $this->request = $request;
    }

    final public function calculateDiscount($product): decimal
    {
      $discountvalue = 0;
      $couponcode = null;
      $pricebefore = 0;
      $priceafter = 0;
      $quantity = $this->request->quantity;

      if ($product->category->hasDiscounts()||$product->hasDiscounts())
      {
          $discountvalue=$product->discounts->first()->discount_value;
          $couponcode=$product->discounts->first()->coupon_code;
          $discount_unit=$product->discounts->first()->discount_unit;
          $price=$product->prices->first()->base_price;
          if ($discount_unit="percentage")
          {
              $pricebefore = $price * $quantity;
              $priceafter = $pricebefore - ($discountvalue/100);
              echo "The product have discount";
          }
          else {
              $pricebefore = $price * $quantity;
              $priceafter = $pricebefore - $discountvalue;
              echo "The product have discount";
          }
      }
      else {
          $priceafter = $pricebefore;
          echo "The product don't have discount";
          return
      }
    }

    /**
     * @return string
     */

    abstract protected function typeDiscount($couponcode): string;
}
