<?php

namespace App\Services\Template;

class DiscountDate extends TemplateAbstract
{

    protected function typeDiscount($couponcode): string
    {
        $typeDiscount = "";

        //consultar si el cupon tiene fechas registradas para aplicar el descuento
        $model=ProductCategoryDiscount::where('coupon_code',$couponcode)->first();
        if (($model->date_created!="")&&($model->valid_until!=""))
        {
            $typeDiscount = "DiscountDateCategory";
        }
         return $typeDiscount;
    }

}
