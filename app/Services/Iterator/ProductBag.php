<?php
/**
 * Cart Products Bag
 */

namespace App\Services\Iterator;

/**
 * This class defines a bag (multi element set or array) of single cart products
 *
 * @see Product
 */
class ProductBag implements \IteratorAggregate, \Countable
{
    /**
     * Product storage
     *
     * @see Product
     *
     * @var array
     */
    protected $products;

    /**
     * Constructor
     *
     * @param array $Products An array of Product
     */
    public function __construct(array $products = array())
    {
        $this->replace($products);
    }

    /**
     * Return all the products
     *
     * @see Item
     *
     * @return array An array of Products
     */
    public function all()
    {
        return $this->products;
    }

    /**
     * Replace the contents of this bag with the specified Products
     *
     * @see Item
     *
     * @param array $items An array of Products
     */
    public function replace(array $products = array())
    {
        $this->products = array();

        foreach ($products as $product) {
            $this->add($product);
        }
    }

    /**
     * Add an product to the bag
     *
     * @see Product
     *
     * @param ProductInterface|array $item An existing item, or associative array of item parameters
     */
    public function add($product)
    {
        if ($product instanceof ItemInterface) {
            $this->$products[] = $product;
        } else {
            $this-$products[] = new Producto($product);
        }
    }

    /**
     * Returns an iterator for products
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->products);
    }

    /**
     * Returns the number of products
     *
     * @return int The number of products
     */
    public function count()
    {
        return count($this->products);
    }
}
