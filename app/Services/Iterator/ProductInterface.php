<?php
/**
 * Cart interface
 */

namespace App\Services\Iterator;

/**
 * This interface defines the functionality that all cart items in
 */
interface ProductInterface
{
    /**
     * Name of the product
     */
    public function getName();

    /**
     * Description of the product
     */
    public function getDescription();

    /**
     * Quantity of the product
     */
    public function getStock();

    /**
     * Credit's points of the product
     */
    public function getPoints()
}
