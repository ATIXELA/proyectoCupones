<?php

namespace App\Services\Iterator;

use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @see ItemInterface
 */
class Product implements ProductInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    protected $parameters;

    /**
     *
     * @param array|null $parameters An array of parameters to set on the new object
     */
    public function __construct(array $parameters = null)
    {
        $this->initialize($parameters);
    }

    /**
     * @param array|null $parameters An array of parameters to set on this object
     * @return $this Item
     */
    public function initialize(array $parameters = null)
    {
        $this->parameters = new ParameterBag;

        Helper::initialize($this, $parameters);

        return $this;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters->all();
    }

    protected function getParameter($key)
    {
        return $this->parameters->get($key);
    }

    protected function setParameter($key, $value)
    {
        $this->parameters->set($key, $value);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return $this->getParameter('product_name');
    }

    /**
     * Set the item name
     */
    public function setName($value)
    {
        return $this->setParameter('product_name', $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription()
    {
        return $this->getParameter('product_description');
    }

    /**
     * Set the item description
     */
    public function setDescription($value)
    {
        return $this->setParameter('product_description', $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getStock()
    {
        return $this->getParameter('units_in_stock');
    }

    /**
     * Set the item quantity
     */
    public function setStock($value)
    {
        return $this->setParameter('units_in_stock', $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getPoints()
    {
        return $this->getParameter('reward_points_credit');
    }

    /**
     * Set the item price
     */
    public function setPoints($value)
    {
        return $this->setParameter('reward_points_credit', $value);
    }
}
