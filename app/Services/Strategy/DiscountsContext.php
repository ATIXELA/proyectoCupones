<?php

namespace App\Services\Strategy;

class DiscountsContext
{
    /**
    * @var CalculatorTypeDiscountInterface
    */
   private var $calcTypeDiscount;

    public function __construct(CalculatorTypeDiscountInterface $calcTypeDiscount)
    {
        $this->calcTypeDiscount = $calcTypeDiscount;
    }

    public function verifyConditions()
    {
        $this->calcTypeDiscount->verifyConditions();
    }

}
