<?php

namespace App\Services\Strategy;

interface CalculatorTypeDiscountInterface
{
    /**
    * @return string
    */

    public function verifyConditions(): string;

}
