<?php

namespace App\Services\Strategy;

use App\Product;
use Illuminate\Http\Request;

class CalcTypeDiscountDate implements CalcTypeDiscountInterface
{
    /**
    * @var Product
    */
    private $product;
    /**
     * @var Request
     */
    private $request;

    /**
     * CalculatorTypeDiscountDate constructor.
     * @param Product $product
     * @param Request $request
     */
    public function __construct(Product $product, Request $request)
    {
        $this->product = $product;
        $this->request = $request;
    }
    public function verifyConditions(): string
    {
        $verifyConditions = '';
        $fechaActual = date('d/m/Y');
        $couponcode=$product->discounts->first()->coupon_code;

        //verify coupon according Product
        $model=ProductDiscount::where('coupon_code',$couponcode)->first();
        $date_created = $model->date_created;
        $valid_until = $model->valid_until;
        if (($date_created>=$fechaActual)&&($fechaActual=<$valid_from))
        {
            $verifyConditions = "The product comply the conditions to apply the discount";
        }
        else {
            $verifyConditions = "The product don't comply the conditions to apply the discount"
        }
        return $verifyConditions;
    }

}
