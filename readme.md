## Descripción del problema

Realizar una aplicación que permita generar descuentos por compra en el prototipo de una tienda de e-commerce utilizando el modelo de base de datos proporcionado y aplicando en la codificación al menos 3 patrones de diseño de comportamiento y/o estructurales.

El tipo de descuento que se va a manejar es mediante cupón, utilizando solamente esa parte del modelo de base de datos (Azul)

## RESTRICCIONES DEL DESCUENTO POR CUPÓN

-Debe permitir un descuento por porcentaje
-Debe permitir un descuento por periodos (fechas-horas)
-Debe permitir hacer descuento por categoría de productos
-Cupón solo válido para una sola compra
-El cupón debe permitir un tope máximo por compra de productos (cantidad monetaria) o por compra mínima. Ej. En la compra de $1000 se aplica en 10% de descuento

*Queda fuera el cálculo de impuestos.

## Solución

Para la solución se aplicaron 3 patrones de diseño.
Se utilizó el Patron TemplateMethod para determinar el cálculo del producto después del descuento según sea el descuento por porcentaje o valor unitario y definir si el producto tiene un descuento por categoría, por producto, por fechas y por cantidad.
Para saber si el producto cumple con los requisitos para aplicar el descuento se uso el patrón Strategy.
El patron Iterator nos permite recorrer los productos, para poder añadirlos a un ProductBag (carrito de compras) simulando la compra

Todos los patrones están dentro del directorio App/Services
