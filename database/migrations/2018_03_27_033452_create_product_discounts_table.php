<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->decimal('discount_value');
            $table->string('discount_unit',20);
            $table->date('date_created');
            $table->date('valid_until');
            $table->string('coupon_code',10);
            $table->decimal('minimum_order_value');
            $table->decimal('maximum_discount_amount');
            $table->boolean('is_redeem_allowed');

            $table->foreign('product_id')->references('id')
                  ->on('products')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_discounts');
    }
}
