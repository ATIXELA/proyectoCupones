<?php

use Faker\Generator as Faker;

$factory->define(App\ProductPricing::class, function (Faker $faker) {
    return [
        'base_price' => 150.00,
        'date_created' => $faker->date(),
        'date_expiry' => $faker->date(),
        'is_active' => true,
    ];
});
