<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'product_name' => $faker->word,
        'product_description' => $faker->paragraph,
        'units_in_stock' => $faker->randomDigit,
        'product_category_id' => function() {
            factory(\App\ProductCategory::class)->create();
        },
        'reward_points_credit' =>rand(1,50),
    ];
});
