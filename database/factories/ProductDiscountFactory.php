<?php

use Faker\Generator as Faker;

$factory->define(App\ProductDiscount::class, function (Faker $faker) {
    return [
        'product_id' => null,
        'discount_value' => 20,
        'discount_unit' => '%',
        'date_created' => $faker->date(),
        'valid_until' => $faker->date(),
        'coupon_code' => $faker->TESTCOUPON,
        'minimum_order_value' => 10,
        'maximum_discount_amount' => 100,
        'is_redeem_allowed' => true,
    ];
});
