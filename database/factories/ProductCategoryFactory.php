<?php

use Faker\Generator as Faker;

$factory->define(App\ProductCategory::class, function (Faker $faker) {
      $categories = [
          "Televisores",
          "Celulares",
          "Computadoras"
      ];
      return [
        'category_name' => $categories
        [array_rand($categories,1)],
        'max_reward_points_encash'=>rand(10,20),
      ];
});
