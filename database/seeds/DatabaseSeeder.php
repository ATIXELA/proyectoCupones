<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        factory(\App\Product::class)->create([
            'product_name' => '55" MU6103 Smart 4K UHD TV',
            'product_description' => 'El verdadero TV 4K UHD. Resolución UHD. PurColour',
            'units_in_stock' => 50,
            'product_category_id' => function() {
                return factory(\App\ProductCategory::class)->create([
                    'category_name' => 'Televisores'
                ])->id;
            },
        ])->each(function($p) {
            $p->prices()->create([
                'base_price' => '900.00',
                'date_created' => '2017-12-30',
                'date_expiry' => '2018-12-30',
                'is_active' => true,
            ]);
            $p->discounts()->create([
                'discount_value' => 5,
                'discount_unit' => '%',
                'date_created' => '2018-03-15',
                'valid_until' => '2018-04-05',
                'coupon_code' => 'TESTCOUPON-005',
                'minimum_order_value' => 0,
                'maximum_discount_amount' => 15,
                'is_redeem_allowed' => true,
            ]);
        });

        factory(\App\Product::class)->create([
          'product_name' => 'Notebook 7 spin 15.6" (12GB RAM)',
          'product_description' => 'Windows 10 Home, 7th Gen Intel® Core™ i7 Processor, 15.6" LED Display (1920x1080 dots), 1TB (HDD) Storage ',
          'units_in_stock' => 120,
          'product_category_id' => function() {
              return factory(\App\ProductCategory::class)->create([
                  'category_name' => 'Computadoras'
              ])->id;
          },
          ])->each(function($p) {
              $p->prices()->create([
                  'base_price' => '1100.00',
                  'date_created' => '2017-07-01',
                  'date_expiry' => '2018-02-01',
                  'is_active' => true,
              ]);
              $p->discounts()->create([
                  'discount_value' => 12,
                  'discount_unit' => '%',
                  'date_created' => '2018-12-20',
                  'valid_until' => '2018-04-20',
                  'coupon_code' => 'TESTCOUPON-012',
                  'minimum_order_value' => 0,
                  'maximum_discount_amount' => 0,
                  'is_redeem_allowed' => false,
              ]);
          });

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
